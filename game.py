import sys

def options():
    print("Sorry there's nothing here right now!")
    menu()

def menu():
    choice = input("Welcome to the game!\n"
                   "Please Enter 1, 2 or 3.\n"
                   "1. Play\n"
                   "2. Options\n"
                   "3. Quit")

    if '1' in choice:
        game()

    elif '2' in choice:
        options()

    elif '3' in choice:
        sys.exit()

    elif '4' or 'die' in choice:
        died("Oops! You died! And found a secret function! :P")
        return

    else:
        print("I do not understand! Please try again.")
        menu()

def died(why):
    print("{}You lose! Better luck next time!".format(why))
    PlayAgain = input("Would you like to play again? (Y/N)")
    if 'Y' in PlayAgain:
        game()
    elif 'N' in PlayAgain:
        sys.exit()
    else:
        print("please enter an input I understand!")
        return

def win(why):
    print("{}You win the game! Go You!".format(why))
    PlayAgain = input("Would you like to play again? (Y/N)")
    if 'Y' in PlayAgain:
        game()
    elif 'N' in PlayAgain:
        sys.exit()
    else:
        print("please enter an input I understand!")
        return


def name():
    name = input("Hello! What is your name?")
    print("Welcome to the game {}!".format(name.upper()))
    game()

def game():
    action = input("You are standing at an intersection on a dirt road,\n"
                 "A crossroads sign above you states\n"
                 "CASTLE NORTH\n"
                 "CANYON SOUTH\n"
                 "SWAMP EAST \n"
                 "TOWN WEST\n"
                 "Enter (N/S/E/W)")

    if 'N' in action:
        print("You travel north to the castle\n"
              "Getting there just in time for the festival!\n"
              "You see an old friend in the crowd and rush towards them\n"
              "Turns out it was not an old friend and you trip and fall into them\n"
              "Uh oh! It was the king!")
        died("You are the main event at the festival\n"
             "All the townspeople throw rocks at you for fun\n"
             "Long Live the King!\n")

    elif 'E' in action:
        print("You travel east towards the swamp\n"
              "Your feet start to get bogged down in the mud\n"
              "You get to the point where you feel like you can't escape!\n"
              "Your vision starts to fade and black out\n"
              "You wake up.. to the smell of waffles?\n"
              "You open your eyes, still blurry.. when you hear soft whispers in your ear")
        died("Shrek is Love.. Shrek is Life\n")

    elif 'W' in action:
        print("You start heading west towards the town\n"
              "You get there and notice that the townspeople look at you and turn the other way\n"
              "Soon you realize why\n"
              "You see your face on the local wanted poster!\n"
              "Stop in the name of the law!")
        died("You wake up in jail with Valen Dreth\n")

    elif 'S' in action:
        print("You head south towards the canyon\n"
              "Finally you are here\n"
              "You are finally able to accomplish your life long dream")
        win("you jump off into the canyon\n"
            "you feel like you can fly for a second\n"
            "Where am I...? You say.\n"
            "You are in the House of Elrond.\n"
            "And it is 10:00 in the morning on October 24th, if you want to know.\n"
            "Gandalf! you say.\n"
            "Yes, I am here. And you're lucky to be here too.\n"
            "A few more hours and you would have been beyond our aid.\n"
            "But you have some strength in you, my dear Hobbit.\n")
        menu()

    else:
        print("Wrong input, try again!")
        game()

def main():
    menu()

if __name__ == '__main__':
    main()
